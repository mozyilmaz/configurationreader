﻿using ConfigurationReader.Data;
using StackExchange.Redis;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConfigurationReader
{
    public class ConfiturationReader
    {
        private static string _applicationName;
        static ConfigurationRepository _mongoDbRepo = new ConfigurationRepository("mongodb://localhost:27017");

        private static Lazy<ConnectionMultiplexer> redis = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect("localhost");
        });

        IDatabase cache;

        public ConfiturationReader(string applicationName, string connectionString, int refreshTime)
        {
            _applicationName = applicationName;
            LoadAllKeys();
        }

        void LoadAllKeys()
        {
            //Get list of keys for application from db
            var result = _mongoDbRepo.GetAllConfigurationValues().GetAwaiter().GetResult();

            //Load keys to cache
            cache = redis.Value.GetDatabase();

            foreach (ConfigurationValue item in result)
            {
                cache.HashSet(item.Name, new HashEntry[] { new HashEntry("Type", item.Type), new HashEntry("Value", item.Value) });
            }
        }

        T GetValue<T>(string key)
        {
            if (cache.HashExists(key,"Value"))
            {
                return (T)Convert.ChangeType(cache.StringGet(key), typeof(T));
            }

            throw new InvalidOperationException("Key not found");
        }
    }
}
