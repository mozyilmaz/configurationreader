﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConfigurationReader.Data
{
    public class ConfigurationValue
    {
        [BsonElement("applicationName")]
        public string ApplicationName { get; set; }
        [BsonElement("isActive")]
        public bool IsActive { get; set; }
        [BsonElement("value")]
        public string Value { get; set; }
        [BsonElement("type")]
        public string Type { get; set; }
        [BsonElement("name")]
        public string Name { get; set; }
        public Guid ConfId { get; set; }
        public ObjectId Id { get; set; }
    }
}
