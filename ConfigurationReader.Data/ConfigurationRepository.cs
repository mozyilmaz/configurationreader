﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationReader.Data
{
    public class ConfigurationRepository
    {
        private IMongoClient _client;
        private IMongoDatabase _database;
        private IMongoCollection<ConfigurationValue> _valueCollection;

        public ConfigurationRepository(string connectionString)
        {
            _client = new MongoClient(connectionString);
            _database = _client.GetDatabase("admin");
            _valueCollection = _database.GetCollection<ConfigurationValue>("values");
        }

        public async Task<List<ConfigurationValue>> GetAllConfigurationValues()
        {
            return await _valueCollection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<ConfigurationValue> GetConfigurationValueByKey(string fieldValue)
        {
            var filter = Builders<ConfigurationValue>.Filter.Eq("name", fieldValue);
            var result = await _valueCollection.Find(filter).FirstOrDefaultAsync();
            return result;
        }

        public async Task InsertConfigurationValue(ConfigurationValue c)
        {
            c.ConfId = Guid.NewGuid();
           await _valueCollection.InsertOneAsync(c);
        }

        public async Task<bool> DeleteConfigurationValue(Guid id)
        {
            var filter = Builders<ConfigurationValue>.Filter.Eq("ConfId", id);
            var result = await _valueCollection.DeleteOneAsync(filter);
            return result.DeletedCount != 0;
        }
    }
}
